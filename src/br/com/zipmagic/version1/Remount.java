package br.com.zipmagic.version1;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.commons.io.FileUtils;

public class Remount {

	private static String valor = "saida/exemplo1.zip__/exemplo1/com/inside1.zip__/inside1/br/proximo.zip__/proximo";
	
	public Remount(){
		
	}
	
	public static void mountAll(){
		
		
		
		
	}
	
	public List<String> getAllCompressFileName(String path){
		
		List<String> nameList = new ArrayList<String>();
		
		
		
		
		
		
		
		return nameList;
	}
	
	public static List<String> splitMountPath(String path){
		
		List<String> nameList = new ArrayList<String>();
		
		try {
			String[] blocos = null;
			blocos = path.split("/");
			int y = blocos.length;
			System.out.println("Tamanho "+y);
			for(int k = 0 ; k <= blocos.length - 1; k++){
				//System.out.println(blocos[k]);
				if(blocos[k].endsWith("zip__")){
					//System.out.println("-->"+blocos[k]);
					nameList.add(blocos[k]);
				}
				
			}
			
		} catch (Exception e) {
			System.out.println("Erro: "+e.getMessage());
			e.getStackTrace();
		}
		return nameList;
	}
	
	public static List<String> getListOfFiles(String path){
		
		List<String> list = new ArrayList<String>();
		try {
			File folder = new File(path);
			File[] listOfFiles = folder.listFiles();
			for (int i = 0; i < listOfFiles.length; i++) {
				if(listOfFiles[i].isFile()){
					list.add(listOfFiles[i].getName());
			       // System.out.println("File " + listOfFiles[i].getName());
				}else if (listOfFiles[i].isDirectory()){
					list.add(listOfFiles[i].getName());
					//System.out.println("Directory "+listOfFiles[i].getName());
				}
				
			}
			
		} catch (Exception e) {
			e.getStackTrace();
		}
		
		return list;
		
	}
	
	
	public static int getListNumber(String path){
		
		//List<String> list = new ArrayList<String>();
		int num = -1;
		try {
			String[] blocos = null;
			blocos = path.split("/");
			num = blocos.length;
		} catch (Exception e) {
			e.getStackTrace();
		}
		
		return num;
		
	}
	
	
	public static void remountFile(List<String> filesPkg, int numPath, String path) throws FileNotFoundException, IOException{
		
		FileOutputStream fos = new FileOutputStream("atest.zip");
		ZipOutputStream zos = new ZipOutputStream(fos);
		for(int i = 0; i <= numPath - 1; i++){
			
			addToZipFile(path, zos);
			List<String> u = getListOfFiles(path);
			String path_ = takeOffFirstName(path);
		
			
		}
		
		
	}
	
	public static void addToZipFile(String fileName, ZipOutputStream zos) throws FileNotFoundException, IOException {

		System.out.println("Writing '" + fileName + "' to zip file");

		File file = new File(fileName);
		FileInputStream fis = new FileInputStream(file);
		ZipEntry zipEntry = new ZipEntry(fileName);
		zos.putNextEntry(zipEntry);

		byte[] bytes = new byte[1024];
		int length;
		while ((length = fis.read(bytes)) >= 0) {
			zos.write(bytes, 0, length);
		}

		zos.closeEntry();
		fis.close();
	}
	/**
	 * retira primeiro nome.
	 * @param path
	 * @return
	 */
	public static String takeOffFirstName(String path){
			String newPath = null;
		try {
			if(!path.isEmpty()){
				int num = path.lastIndexOf(File.separator) + 1;
				newPath = path.substring(num, path.length());
			}
		} catch (Exception e) {
			e.getStackTrace();
		}
		return newPath;
	}
	
	/**
	 * retira primeiro nome.
	 * @param path
	 * @return
	 */
	public static String takeOffPathName(String path){
			String newPath = null;
		try {
			if(!path.isEmpty()){
				int num = path.lastIndexOf(File.separator);
				newPath = path.substring(0, num);
			}
		} catch (Exception e) {
			e.getStackTrace();
		}
		return newPath;
	}

	/**
	 * retira primeiro nome.
	 * @param path
	 * @return
	 */
	public static String takeOffPathName2(String path){
			String newPath = null;
		try {
			if(!path.isEmpty()){
				int num = path.lastIndexOf("/");
				newPath = path.substring(0, num);
			}
		} catch (Exception e) {
			e.getStackTrace();
		}
		return newPath;
	}
	public static void main(String[] args) throws IOException{
		int num = getListNumber("saida/exemplo1.zip__/exemplo1/com/inside1.zip__/inside1/br/proximo.zip__/proximo");
		//System.out.println("Blocos: "+num);
		
		List<String> m = new ArrayList<String>(); 
		m = getListOfFiles("saida/exemplo1.zip__/exemplo1"
				+ "/com/inside1.zip__/inside1/br/proximo.zip__/proximo");
		//System.out.println("Numero Arquivos: "+m.size());
		
		String path = "saida/exemplo1.zip__/"
				+ "exemplo1/com/inside1.zip__/inside1/br/proximo.zip__/proximo";
		
		File foutput = new File("output");
		if(!foutput.exists()){
			foutput.mkdir();
		}
		
		
		for(int i = 0 ; i <= num - 1; i++){
			List<String> list = new ArrayList<String>();
			 
			list = getListOfFiles(path);

			
			for(String k : list){
						File file = new File(path+"/"+k);
						if(file.isDirectory()){
									
									File newFile = new File(foutput+"/"+k);
									
									if(list.size() == 1){
										saveAndBring(newFile.toString());
									}else{
										newFile.mkdir();
									}
									
									
									
									
									//System.out.println("diretorio: "+file);
						
						}else if(file.isFile()){
							
									File newFile = new File(foutput+"/"+k);
									try {
										newFile.createNewFile();
									} catch (IOException e) {						
										e.printStackTrace();
									}
								//	if()
									
								//	System.out.println("file: "+file);
						}
						//System.out.println("arquivo: "+k);
			}//fim do for
			
			
			path = takeOffPathName2(path);
			
		}
		
		
		
		
		
		//List<String> pkgList = new ArrayList<String>();
		//pkgList = splitMountPath("saida/exemplo1.zip__/exemplo1/com/inside1.zip__/inside1/br/proximo.zip__/proximo");
	}
	
	
	public static void saveAndBring(String pathFileDestiny){
		
		try {
			System.out.println(new File(".").getCanonicalPath());
			
				String pathBase = new File(".").getCanonicalPath();
				
				String path = takeOffPathName(pathFileDestiny);
				File path_ = new File(path);
				String[] list = path_.list();
				File f = new File(pathFileDestiny);
				f.mkdir();
				for(int x = 0; x <= list.length; x++){
					copyFileUsingApacheCommonsIO(new File(pathBase+File.separator+path+File.separator+list[x]) ,new File(pathBase+File.separator+pathFileDestiny));
				}
				
				
				
				//File path_ = new File(path);
				
			
		} catch (Exception e) {
			e.getStackTrace();
		}
		
		//1o - listar todos arquivos
		
		//2o - criar diretorio
		
		//3o - salvar todos os arquivos dentro do diretorio
		
	}
	private static void copyFileUsingApacheCommonsIO(File source, File dest) throws IOException {
	    FileUtils.copyFile(source, dest);
	}
	private static void copyFileUsingStream(File source, File dest) throws IOException {
	    InputStream is = null;
	    OutputStream os = null;
	    try {
	        is = new FileInputStream(source);
	        os = new FileOutputStream(dest);
	        byte[] buffer = new byte[1024];
	        int length;
	        while ((length = is.read(buffer)) > 0) {
	            os.write(buffer, 0, length);
	        }
	    } finally {
	        is.close();
	        os.close();
	    }
	}
}
