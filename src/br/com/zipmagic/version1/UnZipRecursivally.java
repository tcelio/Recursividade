package br.com.zipmagic.version1;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
/**
 *	Esta classe faz o trabalho basico de desempacotar um 
 *  arquivo zipado ate acabar os arquivos zip, deletandos os mesmos
 * e sobrando apenas os arquivos nao zipados. 
 * @author thiago
 *
 */
public class UnZipRecursivally {
	//List<String> fileList;
	private static final String INPUT_ZIP_FILE = "exemplo1.zip";
	private static final String OUTPUT_FOLDER = "saida";

	public static void main(String[] args) {
		UnZipRecursivally unZip = new UnZipRecursivally();
		unZip.unZipIt(INPUT_ZIP_FILE, OUTPUT_FOLDER);
	}

	/**
	 * Unzip it
	 * 
	 * @param zipFile
	 *            input zip file
	 * @param output
	 *            zip file output folder
	 */
	public void unZipIt(String zipFile, String outputFolder) {

		byte[] buffer = new byte[1024];

		try {

			File folder = new File(OUTPUT_FOLDER);
			if (!folder.exists()) {
				folder.mkdir();
			}

			ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFile));
			ZipEntry        ze = zis.getNextEntry();
			String fileName    = null;
			String directory   = null;

			while (ze != null) {

				if (ze.isDirectory()) {
						directory = ze.getName();
						System.out.println("Diretorio:" + ze.getName());
	
						File newFile = new File(outputFolder + File.separator + directory);
						if (!newFile.exists()) {
							newFile.mkdir();
						}
						ze = zis.getNextEntry();

				} else {
						fileName = ze.getName();
						System.out.println("Arquivo: " + ze.getName());
						File newFile = new File(outputFolder + File.separator + fileName);
						FileOutputStream fos = new FileOutputStream(newFile);
	
						int len;
						while ((len = zis.read(buffer)) > 0) {
							fos.write(buffer, 0, len);
						}
	
						fos.close();
						ze = zis.getNextEntry();
						if (fileName.endsWith("zip")) {
							unZipIt(outputFolder + File.separator + fileName, outputFolder + File.separator + directory);
						}

				}
			}
			File fileDelete = new File(zipFile);
			fileDelete.delete();
			zis.closeEntry();
			zis.close();

			System.out.println("Done");

		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
}