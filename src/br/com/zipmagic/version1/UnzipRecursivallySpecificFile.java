package br.com.zipmagic.version1;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class UnzipRecursivallySpecificFile {
	List<String> fileList;
	private static final String INPUT_ZIP_FILE = "exemplo1.zip";
	private static final String OUTPUT_FOLDER = "saida";
	private static final String pacoteZip = /* "/home/thiago/_WORKSPACE/teste/exemplo1/com/*/ "proximo.zip";
	private static final String file = "arquivo1.txt";
	private String LAST_FILE_STOP = ""; 

	public UnzipRecursivallySpecificFile(){
		this.LAST_FILE_STOP = "";
	}
	public static void main(String[] args) {
	    UnzipRecursivallySpecificFile unZip = new UnzipRecursivallySpecificFile();
	    unZip.unZipIt(INPUT_ZIP_FILE, OUTPUT_FOLDER, pacoteZip, file);
	}

	/**
	 * Unzip it
	 * 
	 * @param zipFile
	 *            input zip file
	 * @param output
	 *            zip file output folder
	 */
	public void unZipIt(String zipFile, String outputFolder, String pacoteZip, String file) {

		byte[] buffer = new byte[1024];
		//String ext = null;
		boolean firstFlow = true;
		//if()
	

		try {

			File folder = new File(OUTPUT_FOLDER);
			if (!folder.exists()) {
				folder.mkdir();
			}

			ZipInputStream zis = new ZipInputStream(new FileInputStream(zipFile));
			ZipEntry        ze = zis.getNextEntry();
			String fileName    = null;
			String directory   = null;
			//String zipFile ze.getName(); 
			System.out.println("-->"+ze.getName());
			
//			if(this.FILE_STOP.matches(pacoteZip) && !ze.getName().matches(pacoteZip)){
//				System.out.println("SAIRRRRR!!!");
//				exit();
//			}	
			
			int ultimaBarra = zipFile.lastIndexOf("/") + 1;
			String fileZiped = zipFile.substring(ultimaBarra, zipFile.length()); 
			
			File newFile666 = new File(outputFolder + File.separator + fileZiped+"__");
			if (!newFile666.exists()) {
				newFile666.mkdir();
				outputFolder = newFile666.toString();
			}
			
			while (ze != null) {

				if (ze.isDirectory()) {
						directory = ze.getName();
						System.out.println("Diretorio:" + ze.getName());
						File newFile = null;
						newFile = new File(outputFolder + File.separator + directory);
						if (!newFile.exists()) {
							newFile.mkdir();
						}
						ze = zis.getNextEntry();

				} else{
						fileName = ze.getName();
						System.out.println("Arquivo: " + ze.getName());
						File newFile = new File(outputFolder + File.separator + fileName);
						
						
						FileOutputStream fos = new FileOutputStream(newFile);
	
						int len;
						while ((len = zis.read(buffer)) > 0) {
							fos.write(buffer, 0, len);
						}
						fos.close();
						ze = zis.getNextEntry();
						
						if (fileName.endsWith("zip")) {

							//rotina criada para que o haja um ultimo flow no arquivo zipado!	
							if(this.LAST_FILE_STOP.matches(pacoteZip)){
								System.out.println("SAIRRRRR!!!");
								exit();
							}else{
								int barra = fileName.lastIndexOf("/") + 1;
								this.LAST_FILE_STOP = fileName.substring(barra, fileName.length());
								unZipIt(outputFolder + File.separator + fileName, outputFolder + File.separator + directory, pacoteZip, file);
							}
							
						}
					

				}
				
				
			}
//					if(!fileName.isEmpty()){
//						int barra = fileName.lastIndexOf("/") + 1;
//						fileName = fileName.substring(barra, fileName.length());	
//					}
//					if(pacoteZip.matches(fileZiped)){
//						System.out.println("SAIRRRRR!!!");
//						exit();
//					}
			
		
				File fileDelete = new File(zipFile);
				fileDelete.delete();
				
			
			zis.closeEntry();
			zis.close();
			System.out.println("Done");

		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
	public static void exit(){
		System.exit(0);
	}
}