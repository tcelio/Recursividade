package com.recursividade.principal;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

public class Tentativa1 {

	public static void main(String[] args) throws ZipException, IOException {
		
		File directory = new File("C:\\Users\\tpereirac\\Desktop\\Servidor 105\\testeNovo\\Recursividade\\Resultado");
		File file = new File("C:\\Users\\tpereirac\\Desktop\\Servidor 105\\testeNovo\\Recursividade\\Teste1.zip");
		ZipFile file_ = new ZipFile(file);
		unzip(file_, directory);
		
		
	}

	
	public static void unzip(final ZipFile zipfile, final File directory)
		    throws IOException {

		    final Enumeration<? extends ZipEntry> entries = zipfile.entries();
		    while (entries.hasMoreElements()) {
		        final ZipEntry entry = entries.nextElement();
		        final File file = file(directory, entry);
		        if (entry.isDirectory()) {
		            continue;
		        }
		        final InputStream input = zipfile.getInputStream(entry);
		        try {
		            // copy bytes from input to file
		        } finally {
		            input.close();
		        }
		    }
		}
	
	protected static File file(final File root, final ZipEntry entry)
		    throws IOException {

		    final File file = new File(root, entry.getName());

		    File parent = file;
		    if (!entry.isDirectory()) {
		        final String name = entry.getName();
		        final int index = name.lastIndexOf('/');
		        if (index != -1) {
		            parent = new File(root, name.substring(0, index));
		        }
		    }
		    if (parent != null && !parent.isDirectory() && !parent.mkdirs()) {
		        throw new IOException(
		            "failed to create a directory: " + parent.getPath());
		    }

		    return file;
		}
}
